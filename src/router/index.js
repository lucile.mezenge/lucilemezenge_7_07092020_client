import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Messages from '../views/Messages.vue'
import CreateAMessage from '../views/CreateAMessage.vue'
import Profile from '../views/Profile.vue'



Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/messages',
    name: 'Messages',
    component: Messages
  },
  {
    path: '/create-message',
    name: 'CreateAMessage',
    component: CreateAMessage
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile
  }
]

const router = new VueRouter({
  routes
})

export default router
