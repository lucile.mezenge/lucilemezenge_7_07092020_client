import Api from '@/services/Api'

export default {
    index () {
        return Api().get('messages')
    },
    createMessage (message) {
        return Api().post('message', message)
    },
    deleteMessage(id) {
        return Api().delete('messages/' + id)
    }
}