// Setup connectors to hit the backend
import axios from 'axios'

export default () => {
    return axios.create({
        baseURL: 'http://localhost:3000/'
    })
}