//hitting the register endpoint in the VUEJS app
import Api from '@/services/Api'

export default {
    // exporte objet qui a une méthode register 
    register(credentials) {
        return Api().post('register', credentials)
    },
    login(credentials) {
        return Api().post('login', credentials)
    },
    deleteAccount(id) {
        return Api().delete('user/' + id)
    }
}